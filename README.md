# zindex

Software libre para indexar y buscar en discos duros que estén desconectados en BASH

## ¿Cómo usarlo?

Simplemente clona este repositorio o descárgalo, abre una terminal en la carpeta que lo tengas y ejecuta el comando para indexar o para buscar. Es importante señalar que se crearán los archivos de registro (txt) en la misma carpeta donde esté zindex.

### Indexar
Para generar los ídices simplemente ejecuta zindex con el parámtero -i
```
./zindex -i
```
zindex solicitará ingreses el número de serie de tu disco duro, no es forzoso pero altamente recomendable para evitar que se sobreescriban particiones con el mismo nombre y para evitar confusiones sobre los discos.

### Buscar
Para buscar en todos los índices generados simplemente ejecuta zindex sin parámetros
```
./zindex
```


## Prerequisitos

Tener instalado kdebase-bin para los cuadros de diálogo y libnotify-bin para las notificaciones

```
sudo apt-get install kdebase-bin libnotify-bin
```

## Versiones

Uso [SemVer](http://semver.org/) para las versiones.

## Autores

* **Lucio Chávez** - *Initial work* - [@luciochavez](https://gitlab.com/luciochavez)

Ve también esta lista de personas [participantes](https://gitlab.com/Zittec/zindex/graphs/master) que han participado en este proyecto.

## Licencia

Este proyecto está liberado bajo una licencia GPLv3 - ve [LICENSE.md](LICENSE.md) para más detalles.
