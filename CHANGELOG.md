# Changelog
Todos los cambios importantes de este proyecto estarán documentados en este archivo.

El formato de este Changelog está basado en [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
y este proyecto se adhiere a [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 1.0 - 2018-08-27
### Added
- Este CHANGELOG.md para ayudar a entender los cambios.
